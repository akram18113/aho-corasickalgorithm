class TrieNode:
    def __init__(self):
        self.children = {}
        self.output = []

class AC:
    def __init__(self, patterns):
        self.root = TrieNode()
        self._build_trie(patterns)
        self._build_failure_links()
        
    def _build_trie(self, patterns):
        for pattern in patterns:
            node = self.root
            for char in pattern:
                if char not in node.children:
                    node.children[char] = TrieNode()
                node = node.children[char]
            node.output.append(pattern)
            
    def _build_failure_links(self):
        queue = []
        for char, node in self.root.children.items():
            queue.append(node)
            node.fail = self.root
        while queue:
            curr = queue.pop(0)
            for char, child in curr.children.items():
                queue.append(child)
                fail = curr.fail
                while fail != self.root and char not in fail.children:
                    fail = fail.fail
                child.fail = fail.children.get(char, self.root)
                child.output += child.fail.output
                
    def search(self, text):
        node = self.root
        matches = []
        for i, char in enumerate(text):
            while node != self.root and char not in node.children:
                node = node.fail
            if char in node.children:
                node = node.children[char]
            matches += [(i-len(match)+1, match) for match in node.output]
        return matches
